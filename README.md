# iOS-CI
Shared continues integration pipeline for iOS. 🚀

# Used Libraries
  * [**Danger**](https://danger.systems/ruby) to automatically improve PR reviews and leaving humans to think about harder problems.
  * [**SwiftLint**](https://github.com/realm/SwiftLint) A tool to enforce Swift style and conventions.
  * [**Fastlane**](https://fastlane.tools) A tool for iOS developers to automate tedious tasks like generating screenshots, dealing with provisioning profiles, and releasing your application.
  
## Danger features
Following is a list of features which are posted in a comment on PRs based on the submitted files.

  - Warn for if the PR is classed as [WIP] Work in Progress.
  - Warn for big PRs, containing more than 500 lines of code.
  - Warn for missing PR description.
  - Show all `warnings` and `errors` in the project.

### Custom linting
These warnings are posted inline inside the PR.

  - Check for `final class` usage.
  - `override` methods without adding logic.
  - Suggest `weak` over `unowned`.
  - Suggest `// MARK:` usage for large files.
  
![](Assets/danger.png)
   
# Installation

### 1. Setup your environment
To run danger you need: 

  * [**Ruby**](https://www.ruby-lang.org/en/documentation/installation)
  * [**Bundler**](https://bundler.io)

### 2. Add your bitbucket credentials as environment variables
  * DANGER_BITBUCKETCLOUD_USERNAME
  * DANGER_BITBUCKETCLOUD_PASSWORD

### 3. Create a fastlane file
Import the Fastfile from this repo and trigger the `validate_changes` lane.

```ruby
import "./../iOS-CI/Fastlane/Fastfile"

desc "run validate the changes"
lane :test
  validate_changes
end
```

### 4. Run fastlane
Run fastlane using your CI environment.

# Future work

  - Add Warn for missing updated tests.
  - Show code coverage of PR related files.
  - Show any failed tests.
  - Figure out why```swiftlint.lint_files inline_mode: true``` dose not work on bitbucket PRs.
  - Bautify danger output.
  
# Author

  Anas Alhasani
